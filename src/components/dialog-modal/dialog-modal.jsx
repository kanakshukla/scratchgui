import PropTypes from 'prop-types';
import React, { useState } from 'react';
import ReactModal from 'react-modal';
import Box from '../box/box.jsx';
import { injectIntl, intlShape } from 'react-intl';

import styles from './dialog-modal.css';
import downloadIcon from './download_icon.svg';

const DialogModal = (props) => {

    let { IsModalOpen, contentMessage, buttonText, onClose } = props;

    return (

        <ReactModal
            isOpen={IsModalOpen}
            className={styles.modalContent}
            overlayClassName={styles.modalOverlay}
        >
            <div>
                <Box className={styles.illustration}>Information!</Box>

                <Box className={styles.body}>
                    <img src={downloadIcon} />
                    <p>{contentMessage}</p>
                    <Box className={styles.buttonRow}>
                        <button
                            className={styles.backButton}
                            onClick={() => onClose()} >
                            {buttonText}
                        </button>
                    </Box>
                </Box>
            </div>
        </ReactModal>
    );
};

DialogModal.propTypes = {
    IsModalOpen: PropTypes.bool,
    contentMessage: PropTypes.string,
    buttonText: PropTypes.string,
    onClose: PropTypes.func.isRequired
};

DialogModal.defaultProps = {
    IsModalOpen: false
};

const WrappedDialogModal = injectIntl(DialogModal);
WrappedDialogModal.setAppElement = ReactModal.setAppElement;
export default WrappedDialogModal;
